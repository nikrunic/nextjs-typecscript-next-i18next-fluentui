# Grow Viral Front End

A Campaign Manager to manage leads to a campaign.

Please use yarn for the dependencies.

## Install Node Modules

```
yarn
```

## Commands

```sh
# Starts the development server.
yarn dev
```

```sh
# Builds the app for production.
yarn build
```

```sh
# Runs the built app in production mode.
yarn start
```
