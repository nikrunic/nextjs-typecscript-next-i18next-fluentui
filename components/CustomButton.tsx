import React from 'react'
import { PrimaryButton } from '@fluentui/react'

export interface CustomButtonProps {
  disabled?: boolean
  checked?: boolean
  text?: string
}

const CustomButton = ({ ...rest }: CustomButtonProps): JSX.Element => {
  const _alertClicked = (): void => {
    alert('Clicked')
  }

  return <PrimaryButton onClick={_alertClicked} {...rest} />
}

export default CustomButton
